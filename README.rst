envjson
=======
https://pypi.python.org/pypi/envjson

|release|  |lic|

.. |release| image:: https://img.shields.io/pypi/v/envjson.svg
    :target: https://pypi.python.org/pypi/envjson

.. |lic| image:: https://img.shields.io/pypi/l/envjson.svg
    :target: https://pypi.python.org/pypi/envjson


**Parse environments with ease**


Description
-----------

*Sample short description*

Here will be an introductory description.



Documentation
-------------

http://envjson.readthedocs.org/


