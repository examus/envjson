from .env_parser import *


VERSION = (20180731, 1, 1)
"""Application version number tuple."""

VERSION_STR = '.'.join(map(str, VERSION))
"""Application version number string."""