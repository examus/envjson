envjson documentation
=====================
https://pypi.python.org/pypi/envjson



Description
-----------

Parse env strings as json


Requirements
------------

1. Python 3.5+



Table of Contents
-----------------

.. toctree::
    :maxdepth: 2

    quickstart


